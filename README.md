<img src="https://is.muni.cz/www/milan.cermak/granef/granef-logo.svg" height="60px">

**Graph-Based Network Forensics**

---

Understanding the information in captured network traffic, extracting the necessary data, and performing incident investigations are principal tasks that network forensics focuses on. We believe that exploratory data analysis based on associations will prove to be a natural approach to network traffic analysis, allowing us to reveal a context that would remain hidden otherwise. Therefore, we introduce a Granef toolkit that implements this new approach...


### Requirements

The toolkit primarily uses the Docker environment maintained by Python script [granef.py](granef/granef.py). The complete list of requirements is provided below:

- Docker (requires approximately 1.5 GB disk space for Docker images)
- Python3
- Python3 packages in [requirements.txt](requirements.txt)

The installation can be performed using the following commands (install argument "-e" is used to allow dynamic changes in configuration files):

```bash
$ git clone https://gitlab.ics.muni.cz/granef/granef.git
$ pip3 install -e ./granef/
```

Alternatively, the toolkit can be installed in the Python virtual environment using the following commands:

```bash
$ git clone https://gitlab.ics.muni.cz/granef/granef.git
$ python3 -m venv granef-venv
$ source granef-venv/bin/activate
(granef-venv)$ pip3 install -e ./granef/
# Use this environment for granef run
# Deactivate the environment when no longer needed
(granef-venv)$ deactivate
```


### Usage

Modules and data processing operations are defined in the default [granef.yml](/granef/configuration/granef.yml) configuration file. If you want to set up multiple analysis environments, copy the original file and change the operation modules and Docker network names. Use `-c` script argument to load the new configuration file.

The configuration file takes precedence over the scripts' arguments to automate the analysis workflow. For example, remove the `input` key from the selected operation to prefer the `-i` script argument.

The script allows you to fully automate the Docker environment's preparation (pull Docker images and create a network) and run all data processing operations specified in the configuration file. Use the following command to start this automated data processing:

```bash
$ granef -a -i <INPUT_FILE_OR_DIRECTORY_PATH>
```

The Granef toolkit then performs the following steps:

1. Pull all Docker images to the local Docker registry.
2. Create a new Docker network in which all modules will be run.
3. Run all data processing operations and their modules.
    - Each module will create a new directory in the parent directory of `<INPUT_FILE_OR_DIRECTORY_PATH>` named according to the operation name.
    - Modules with the attribute "detached" are run asynchronously and will remain running even after the script ends.

After a successful run of all operations, the following web services will be available (using the default configuration):

- Granef GUI – [http://127.0.0.1:80](http://127.0.0.1:80)
- Granef API – [http://127.0.0.1:7000](http://127.0.0.1:7000)
- Dgraph Alpha – [http://127.0.0.1:8080](http://127.0.0.1:8080)
- Dgraph Ratel is now served as a service at [https://play.dgraph.io/](https://play.dgraph.io/) (insert the Dgraph Alpha address as the Dgraph server URL)

These ports can be changed in the [granef.yml](granef/configuration/granef.yml) configuration file (in the `ports` field of `granef-handling-alpha`, `granef-analysis-api`, and `granef-analysis-web` Docker Images). 

Use the following commands to stop, start, or remove running operation containers:

```bash
$ granef -o <OPERATION_NAME> -t stop
$ granef -o <OPERATION_NAME> -t start
$ granef -o <OPERATION_NAME> -t remove
```

Use the following command to completely clean up the Docker environment and remove all Granef objects:

```bash
$ granef -r all
```

All Granef toolkit actions can be called separately. Use the following commands to pull Docker images, create a new Docker network, run the operation, and remove all running containers of the operation:

```bash
$ granef -p
$ granef -n
$ granef -o <OPERATION_NAME> -t run -i <INPUT_FILE_OR_DIRECTORY_PATH>
$ granef -r operations
```

<br/>

---

### All project contributors

- **Milan Cermak** – Granef idea, guidance and suggestions during development
- **Denisa Sramkova** – prototype implementation, data processing and analysis modules
- **Aneta Jablunkova** – IP flow data processing and analysis modules
- **Tatiana Fritzova** – graphical analysis interface

Pull requests or new modules are welcome! For major changes, please open an issue first to discuss what you would like to add or change.
