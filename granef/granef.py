#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2021  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""Granef toolkit for graph-based network traffic analysis and forensics.

This is a maintainer script for processing network traffic and its analysis using graph database
and object relations. The complete documentation is available at https://gitlab.ics.muni.cz/granef/granef.

Examples:
    Automated workflow that prepares the Docker environment and runs all operations:
        $ granef.py -a -d ./data/ -i trace.pcap
    
    Pull all images to the local Docker registry:
        $ granef.py -p
    
    Create Docker network named according to the configuration:
        $ granef.py -n
    
    Run all containers of operation "transformation":
        $ granef.py -o transformation -t run -d ./data/
    
    Remove all Granef objects from the local Docker registry (images, network, running containers):
        $ granef.py -r all
"""


from asyncio.log import logger
from distutils.log import Log
import sys          # Common system functions
import argparse     # Arguments parser
import yaml         # YAML files parser
import logging, coloredlogs                 # Standard logging functionality with colors functionality
import pathlib      # Directory path functions
import re           # Regular expressions
import docker       # Docker SDK
from pkg_resources import resource_stream   # Package resources access
from docker.client import DockerClient      # Docker SDK – client


def pull_images(docker_client: DockerClient, configuration: dict) -> bool:
    """Pull (download) of all images specified in the configuration to the local Docker repository.

    Args:
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "docker_images" field.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    for image, image_repository in configuration.get("docker_images").items():
        try:
            logger.info("Docker image pull: " + image)
            docker_client.images.pull(image_repository.get("repository"), tag=image_repository.get("tag", "latest"),
                auth_config={"username": image_repository.get("username"), "password": image_repository.get("password")})
        except docker.errors.APIError as exc:
            logger.error("Image repository pull failed: " + str(exc))
            return False
    logger.info("All Docker images pulled successfully")
    return True


def remove_images(docker_client: DockerClient, configuration: dict) -> bool:
    """Remove all Docker images specified in in the configuration from the local Docker repository.

    Args:
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "docker_images" field.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    for image, image_repository in configuration.get("docker_images").items():
        logger.info("Docker image remove: " + image)
        try:
            docker_client.images.remove(image_repository.get("repository") + ":" + image_repository.get("tag", "latest"))
        except docker.errors.ImageNotFound:
            continue        
        except docker.errors.APIError as exc:
            logger.error("Image remove failed: " + str(exc))
            return False
    logger.info("All Docker images have been removed")
    return True


def create_network(docker_client: DockerClient, configuration: dict) -> bool:
    """Create a new Docker network with name specified in the configuration.

    Args:
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "docker_network" field.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    logger.info("Creating network: " + configuration.get("docker_network"))
    try:
        docker_client.networks.create(configuration.get("docker_network"), driver="bridge", check_duplicate=True)
    except docker.errors.APIError as exc:
        if "already exists" in exc.explanation:
            logger.warning("Network " + configuration.get("docker_network") + " already exists")
        else:
            logger.error("Network creation failed: " + str(exc))
            return False
    logger.info("Network " + configuration.get("docker_network") + " created successfully")
    return True


def remove_network(docker_client: DockerClient, configuration: dict) -> bool:
    """Remove a Docker network with name specified in the configuration.

    Args:
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "docker_network" field.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    logger.info("Removing network: " + configuration.get("docker_network"))
    try:
        network_object = docker_client.networks.get(configuration.get("docker_network"))
        network_object.remove()
    except docker.errors.NotFound:
        pass
    except docker.errors.APIError as exc:
        logger.error("Network remove failed: " + str(exc))
        return False
    logger.info("Network " + configuration.get("docker_network") + " has been removed")
    return True


def parse_operation_part_log(operation_part_output: str, operation_part_name:str) -> bool:
    """Parse output from the operation_part and transform it into the main script log.

    Args:
        operation_part_output (str): All lines of the module output.
        operation_part_name (str): Name of the module that generated the log.
    Returns:
        bool: False if the log contains error string, True otherwise.
    """
    without_error = True
    # Create new logger with a name of the module
    module_logger = logging.getLogger(operation_part_name)
    for output_line in operation_part_output.splitlines():
        # Parse log and extract key values
        output_elements = output_line.split(": ",1)
        if len(output_elements) <= 1:
            logger.error("Unable to parse module \"" + operation_part_name + "\" output: " + output_line)
            without_error = False
            continue
        log_message = output_elements[1]
        extracted_level = re.search("\[(.*)\]", output_elements[0])
        if extracted_level:
            # Handle expected logs
            log_level =  getattr(logging, extracted_level.group(1))
        else:
            # Catch any other log messages
            log_level = getattr(logging, "ERROR")
            log_message = output_line
        module_logger.log(log_level, log_message.replace("\\n", "\n"))
        if log_level == getattr(logging, "ERROR"):
            without_error = False
    return without_error


def run_operation(operation: str, args: argparse.ArgumentParser, docker_client: DockerClient, configuration: dict) -> bool:
    """Run all containers of the specified operation according to the given configuration.

    Each operation may have multiple parts (containers) that are run in sequence.
    
    Containers can be run as detached (service) or as regular tasks (not detached). In the case of a task, the container's
    output is logged with level debug.

    Args:
        operation (str): Operation name that match name in the configuration.
        args (argparse.ArgumentParser): Initialized arguments with "args.input" and optionally "args.source" value defined.
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "operations" and "docker_images" fields.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    logger.info("Starting " + operation + " operation parts")
    for operation_part in configuration.get("operations").get(operation):
        image_repository = configuration.get("docker_images").get(operation_part.get("image")).get("repository")
        image_tag = configuration.get("docker_images").get(operation_part.get("image")).get("tag", "latest")
        image_name = image_repository + ":" + image_tag

        # Check if container has been pulled and network exists
        try:
            docker_client.images.get(image_name)
            docker_client.networks.get(configuration.get("docker_network"))
        except docker.errors.ImageNotFound as exc:
            logger.error("Image not found: " + str(exc))
            return False
        except docker.errors.NotFound as exc:
            logger.error("Network not found: " + str(exc))
            return False

        # Get input directory and file/directory name
        input_directory = str(args.input.resolve().parent)
        input_name = operation_part.get("input", args.input.name)
        if "input" in operation_part.keys():
            logger.warning("Using input name from the configuration file: " + (input_name if input_name else "Null"))
        input_argument = "-i " + input_name if input_name else ""

        # Get command, set debug log level, and add source for transformation tasks
        command = "-l debug {command}".format(command=operation_part.get("command", ""))
        if operation == "transformation":
            command = "-s {source} {command}".format(source=args.source, command=command)

        # Run image, set container name, and attach network
        logger.info("Running container '" + operation_part.get("name") + "' from image: " + image_name)
        try:
            container_log = docker_client.containers.run(
                image=image_name,
                name=operation_part.get("name"),
                network=configuration.get("docker_network"),
                detach=operation_part.get("detach", False),
                remove=False if operation_part.get("detach", False) else True,
                command="{input_argument} {command}".format(input_argument=input_argument, command=command),
                volumes={input_directory: {"bind": "/data/", "mode": "rw"}},
                ports={key: tuple(value) for key, value in operation_part.get("ports", {}).items()},
                stdout=True,
                stderr=True
            )
            if not isinstance(container_log, docker.models.containers.Container):
                without_error_check = parse_operation_part_log(container_log.decode(), operation_part.get("name"))
                if not without_error_check:
                    logger.critical("Operation part " + operation_part.get("name") + " failed")
                    return False
        except (docker.errors.ContainerError, docker.errors.APIError) as exc:
            logger.error("Container run failed: " + str(exc))
            return False
    
    logger.info("All " + operation + " operation parts has been started")
    return True


def stop_start_operation(operation: str, task: str, docker_client: DockerClient, configuration: dict) -> bool:
    """Allows to stop or start all containers of the specified operation according to the given configuration.

    Each operation may have multiple parts (containers) whereas on all of them is performed specified task.

    Args:
        operation (str): Operation name that match name in the configuration.
        task (str): "stop" or "start" depending on task to perform.
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "operations" field.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    logger.info("Performing task " + task + " on all " + operation + " operation parts")
    for operation_part in configuration.get("operations").get(operation):
        logger.info("Performing operation " + task + " on container: " + operation_part.get("name"))
        try:
            container_object = docker_client.containers.get(operation_part.get("name"))
            if task == "stop":
                container_object.stop()
            elif task == "start":
                container_object.start()
            else:
                logger.error("Unsupported operation task: " + task)
                return False
        except docker.errors.NotFound as exc:
            logger.warning("Container " + operation_part.get("name") + " not found")
            continue
        except docker.errors.APIError as exc:
            logger.error("Container " + operation + " failed: " + str(exc))
            return False
    logger.info("Task " + task + " on all " + operation + " operation parts has been performed")
    return True


def remove_operation(operation: str, docker_client: DockerClient, configuration: dict) -> bool:
    """Remove of all containers of the specified operation according to the given configuration.

    Each operation may have multiple parts (containers) whereas all of them are removed.

    Args:
        operation (str): Operation name that match name in the configuration.
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "operations" field.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    logger.info("Removing " + operation + " operation parts")
    for operation_part in configuration.get("operations").get(operation):
        logger.info("Removing container: " + operation_part.get("name"))
        try:
            container_object = docker_client.containers.get(operation_part.get("name"))
            container_object.remove(force=True)
        except docker.errors.NotFound as exc:
            logger.warning("Container " + operation_part.get("name") + " not found")
            continue
        except docker.errors.APIError as exc:
            logger.error("Container remove failed: " + str(exc))
            return False
    logger.info("All " + operation + " operation parts has been removed")
    return True   


def remove(object_type: str, docker_client: DockerClient, configuration: dict) -> bool:
    """Remove all Docker objects according to the specified object type.

    The function allows to remove images in local Docker registry, network, or containers for all operations.

    Args:
        object_type (str): Type of the object to remove. Expected values: "images", "network", "operations", "all".
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "operations", "docker_network", and "docker_images" fields.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    if object_type == "images":
        return remove_images(docker_client, configuration)
    elif object_type == "network":
        return remove_network(docker_client, configuration)
    elif object_type == "operations":
        for operation in ["extraction", "transformation", "indexing", "handling", "analysis"]:
            if not remove_operation(operation, docker_client, configuration):
                return False
    elif object_type == "all":
        for operation in ["extraction", "transformation", "indexing", "handling", "analysis"]:
            if not remove_operation(operation, docker_client, configuration):
                return False
        if not remove_images(docker_client, configuration):
            return False
        return remove_network(docker_client, configuration)
    else:
        logger.error("Unsupported remove argument: " + object_type)
        return False
    return True


def operation_task(args: argparse.ArgumentParser, docker_client: DockerClient, configuration: dict) -> bool:
    """Executes specified task on all containers related to the specified operation.

    The function servers as an abstraction all available operation tasks.
    
    Currently available tasks: "run", "stop", "start", "remove".

    Each operation may have multiple parts (containers) whereas the specified task is performed on all of them.

    Args:
        args (argparse.ArgumentParser): Initialized arguments with "args.task", "args.operation", "args.input" values defined.
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "operations" and "docker_images" fields.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    if args.task == "run":
        return run_operation(args.operation, args, docker_client, configuration)
    elif args.task == "stop" or args.task == "start":
        return stop_start_operation(args.operation, args.task, docker_client, configuration)
    elif args.task == "remove":
        return remove_operation(args.operation, docker_client, configuration)
    else:
        logger.error("No operation task specified")
        return False


def action_all(args: argparse.ArgumentParser, docker_client: DockerClient, configuration: dict) -> bool:
    """Execute the whole data processing workflow (pipeline).

    The function will initialize the Docker environment (pull images and set-up the network) and run operations
    in order – "extraction", "transformation", "indexing", "handling", "analysis".

    The function is not prepared as transaction. So if some task will fail, previous completed tasks will not be rolled back.

    Args:
        args (argparse.ArgumentParser): Initialized arguments with "args.input" and optionally "args.source" value defined.
        docker_client (DockerClient): Initialized Docker client.
        configuration (dict): Parsed configuration with "operations", "docker_network", and "docker_images" fields.

    Returns:
        bool: False if some error ocurred, True otherwise.
    """
    logger.info("Docker environment initialization")
    if not pull_images(docker_client, configuration):
        return False
    if not create_network(docker_client, configuration):
        return False

    logger.info("Workflow starting")
    for operation in ["extraction", "transformation", "indexing", "handling", "analysis"]:
        if not run_operation(operation, args, docker_client, configuration):
            return False
    return True


def main():
    """Parse script arguments and perform desired operation.
    """
    # Define application arguments (automatically creates -h argument)
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    parser.add_argument("-c", "--configuration", metavar="CONFIGURATION_FILE", help="Configuration file", required=False, default="granef.yml", type=str)
    group.add_argument("-p", "--pull", help="Pull all images", required=False, action="store_true")
    group.add_argument("-n", "--network", help="Create network", required=False, action="store_true")
    group.add_argument("-r", "--remove", choices=["images", "network", "operations", "all"], help="Remove specified objects",
        required=False, type=str)
    group.add_argument("-o", "--operation", choices=["extraction", "transformation", "indexing", "handling", "analysis"],
        help="Select operation", required=False, type=str)
    group.add_argument("-a", "--all", help="Setup Docker environment and perform all operations", required=False, action="store_true")
    parser.add_argument("-t", "--task", choices=["run", "stop", "start", "remove"], help="Select operation task", required=False,
        default="start", type=str)
    parser.add_argument("-i", "--input", metavar="INPUT_FILE_OR_DIRECTORY_PATH", help="Input file or directory path", required=False, type=pathlib.Path)
    parser.add_argument("-s", "--source", metavar="CASE_NAME", help="Case name stored as a source to each node", required=False, default="granef-case", type=str)
    parser.add_argument("-l", "--log", choices=["debug", "info", "warning", "error", "critical"], help="Log level", required=False, default="INFO")
    args = parser.parse_args()

    # Set logging
    global logger
    logger = logging.getLogger("granef")
    coloredlogs.install(level=getattr(logging, args.log.upper()), fmt="%(asctime)s %(name)s [%(levelname)s]: %(message)s")

    # Load the configuration file
    try:
        configuration = yaml.safe_load(resource_stream(__name__, "configuration/" + args.configuration).read()) # read function allows more detailed error output
        #configuration = yaml.safe_load(args.configuration.read()) # read function allows more detailed error output
    except yaml.YAMLError as exc:
        logger.critical("Configuration file not correctly loaded:\n" + str(exc))
        sys.exit(1)
    
    # Check input argument
    if (args.operation or args.all) and args.input == None:
        logger.critical("Input file or directory must be defined")
        parser.print_help()
        sys.exit(2)
    if args.input and not args.input.exists():
        logger.critical("Input file or directory does not exists: " + str(args.input.resolve()))
        sys.exit(3)
    
    # Initialize local Docker client
    try:
        docker_client = docker.from_env()
    except docker.errors.DockerException as exc:
        logger.critical("Docker environment not found:\n" + str(exc))
        sys.exit(4)
    
    # Process specified action
    if args.pull:
        pull_images(docker_client, configuration)
    elif args.network:
        create_network(docker_client, configuration)
    elif args.remove:
        remove(args.remove, docker_client, configuration)
    elif args.operation:
        operation_task(args, docker_client, configuration)
    elif args.all:
        action_all(args, docker_client, configuration)
    else:
        logger.warning("No action specified")

    docker_client.close()


if __name__ == "__main__":
    main()
