#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2023  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""Granef installer allowing to install it in the system using the pip.

To install it, run the following command ("-e" used to allow correct usage of configuration files):
    $ pip install -e .
"""


from setuptools import setup, find_packages
from typing import List


def read_requirements() -> List[str]:
    """Helper for parsing requirements.txt file.

    Returns:
        List[str]: Array of module names from the requirements.txt file.
    """
    with open("requirements.txt", mode="r", encoding="utf-8") as f:
        return f.read().splitlines()


setup(
    name="granef",
    version="1.1.0",
    author="Milan Cermak",
    packages=find_packages(),
    py_modules=["granef"],
    python_requires=">=3.4",
    include_package_data=True,
    install_requires=read_requirements(),
    entry_points={
        "console_scripts": ["granef=granef.granef:main"]
    }
)
